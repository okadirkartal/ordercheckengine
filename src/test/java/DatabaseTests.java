import com.kartal.dal.DataSeeder;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;

public class DatabaseTests {

    static DataSeeder seeder;

    static String customersDbPath;

    @BeforeClass
    public static void setUp() {

        seeder = new DataSeeder();
        customersDbPath = "data/customers.json";
    }

    @Test
    public void CheckCustomerFile_WhenIsExists_ReturnTrue() {

        seeder.Seed();

        Assert.assertEquals(true, new File(customersDbPath).exists());
    }

    @Test
    public void CheckDatabase_WhenCustomerRecordsExists_ReturnTrue() {

        seeder.Seed();

        Assert.assertEquals(true, new File(customersDbPath).length() > 0);
    }

    @Test
    public void CheckDatabase_WhenCustomerRecordsNotExists_ReturnTrue() {

        seeder.Clear();

        Assert.assertEquals(true, new File(customersDbPath).length() == 0);
    }
}
