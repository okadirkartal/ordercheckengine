import com.kartal.dal.DataSeeder;
import com.kartal.entities.customers.CorporateCustomer;
import com.kartal.entities.customers.PrivateCustomer;
import com.kartal.entities.enums.ValidationResult;
import com.kartal.service.ValidationEngine;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;


public class ValidationEngineTests {

    static DataSeeder seeder;

    PrivateCustomer privateCustomer;

    CorporateCustomer corporateCustomer;

    @BeforeClass
    public static void setUpBeforeClass() {

        seeder = new DataSeeder();

        seeder.Seed();
    }

    @Test
    public void validatePrivateCustomer_CheckInquiry_WhenValidReturnsTrue() {

        privateCustomer = new PrivateCustomer(1l, "11111111111", "United States", "Yuri", "Vladimir", "01/01/1980");

        ValidationEngine<PrivateCustomer> validationEngineForPrivateCustomer
                = new ValidationEngine<>();

        ValidationResult result = validationEngineForPrivateCustomer.InquiryCustomer(privateCustomer);

        Assert.assertSame(ValidationResult.VALID, result);
    }

    @Test
    public void validatePrivateCustomer_CheckIssueCard_WhenValidReturnsTrue() {

        corporateCustomer = new CorporateCustomer(1l, "55555555555", "United States", "MICROSOSFT", "01/01/1990", "818181");

        ValidationEngine<CorporateCustomer> validationEngineForCorporateCustomer
                = new ValidationEngine<>();

        ValidationResult result = validationEngineForCorporateCustomer.IssueCard(corporateCustomer);

        Assert.assertSame(ValidationResult.VALID, result);
    }

    @Test
    public void validatePrivateCustomer_CheckRegistration_WhenValidReturnsTrue() {

        corporateCustomer = new CorporateCustomer(1l, "55555555555", "United States", "MICROSOSFT", "01/01/1990", "343434");

        ValidationEngine<CorporateCustomer> validationEngineForCorporateCustomer
                = new ValidationEngine<>();

        ValidationResult result = validationEngineForCorporateCustomer.RegistrationOfCustomer(corporateCustomer);

        Assert.assertSame(ValidationResult.VALID, result);
    }

    @Test
    public void validateCorporateCustomer_CheckInquiry_WhenValidReturnsTrue() {

        corporateCustomer = new CorporateCustomer(1l, "55555555554", "United States", "MICROSOSFT", "01/01/1990", "818181");

        ValidationEngine<CorporateCustomer> validationEngineForCorporateCustomer
                = new ValidationEngine<>();

        ValidationResult result = validationEngineForCorporateCustomer.InquiryCustomer(corporateCustomer);

        Assert.assertSame(ValidationResult.VALID, result);
    }

    @Test
    public void validateCorporateCustomer_CheckIssueCard_WhenValidReturnsTrue() {

        privateCustomer = new PrivateCustomer(1l, "11111111111", "United States", "Yuri", "Vladimir", "01/01/1980");

        ValidationEngine<PrivateCustomer> validationEngineForPrivateCustomer
                = new ValidationEngine<>();

        ValidationResult result = validationEngineForPrivateCustomer.IssueCard(privateCustomer);

        Assert.assertSame(ValidationResult.VALID, result);
    }

    @Test
    public void validateCorporaCustomer_CheckRegistration_WhenValidReturnsTrue() {

        privateCustomer = new PrivateCustomer(1l, "11111111112", "United States", "Yuri", "Vladimir", "01/01/1980");

        ValidationEngine<PrivateCustomer> validationEngineForPrivateCustomer
                = new ValidationEngine<>();

        ValidationResult result = validationEngineForPrivateCustomer.RegistrationOfCustomer(privateCustomer);

        Assert.assertSame(ValidationResult.VALID, result);
    }

    @AfterClass
    public static void tearDownAfterClass() {

        seeder.Clear();
    }
}
