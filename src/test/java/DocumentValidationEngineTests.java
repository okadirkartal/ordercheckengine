import com.kartal.service.DocumentDataValidationService;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class DocumentValidationEngineTests {

    static DocumentDataValidationService documentDataValidationService;

    @BeforeClass
    public static void setUp() {

        documentDataValidationService = new DocumentDataValidationService();
    }

    @Test
    public void checkCardValidity_WhenValid_ReturnsTrue() {

        Assert.assertTrue(documentDataValidationService.ValidateCard("11111111111"));
    }

    @Test
    public void checkCardValidity_WhenIsValidLength_ReturnsFalse() {

        Assert.assertFalse(documentDataValidationService.ValidateCard("111"));
    }

    @Test
    public void checkCardValidity_WhenIsNull_ReturnsFalse() {

        Assert.assertFalse(documentDataValidationService.ValidateCard(null));
    }

    @Test
    public void checkPlaceName_WhenIsValid_ReturnsTrue() {

        Assert.assertTrue(documentDataValidationService.placeNameIsValid("United States"));
    }

    @Test
    public void checkPlaceName_WhenIsValid_ReturnsFalse() {

        Assert.assertFalse(documentDataValidationService.placeNameIsValid("United StatesX"));
    }
}
