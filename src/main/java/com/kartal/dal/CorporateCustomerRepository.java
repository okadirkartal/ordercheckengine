package com.kartal.dal;

import com.kartal.entities.common.Constants;
import com.kartal.entities.customers.CorporateCustomer;
import org.json.simple.JSONObject;

import java.util.ArrayList;

public class CorporateCustomerRepository extends CustomerRepository<CorporateCustomer> {

    public CorporateCustomerRepository() {
        getCustomers();
    }

    ArrayList<CorporateCustomer> list = new ArrayList<CorporateCustomer>();

    public CorporateCustomer getCustomer(String cardId) {

        return list.stream()
                .filter(search -> cardId.equals(search.get_cardId()))
                .findAny()
                .orElse(null);
    }

    public void getParsedCustomer(JSONObject customer) {

        JSONObject obj = (JSONObject) customer.get(Constants.CorporateCustomerTag);
        if (obj != null)
            list.add(new CorporateCustomer(
                    (long) obj.get("Id"), obj.get("cardId").toString(), obj.get("placeOfRegistration").toString(),
                    obj.get("companyName").toString(), obj.get("foundationDate").toString(), obj.get("registryNumber").toString()
            ));
    }

    public boolean IssueCard(CorporateCustomer customer) {

        return list.stream()
                .filter(search -> customer.get_cardId().toLowerCase().equals(search.get_cardId().toLowerCase()))
                .filter(search -> customer.get_registryNumber().toLowerCase().equals(search.get_registryNumber().toLowerCase()))
                .findAny()
                .orElse(null) != null;
    }

    public boolean RegistrationCustomer(CorporateCustomer customer) {

        CorporateCustomer returnedCustomer = list.stream()
                .filter(search -> customer.get_registryNumber().toLowerCase().equals(search.get_registryNumber().toLowerCase()))
                .findAny()
                .orElse(null);

        if (returnedCustomer != null) {
            System.out.println("this customer is exists");
            return false;
        }
        return true;
    }

    public boolean InquiryCustomer(CorporateCustomer customer) {
        return list.stream()
                .filter(search -> customer.get_registryNumber().toLowerCase().equals(search.get_registryNumber().toLowerCase()))
                .filter(search -> customer.get_placeOfRegistration().toLowerCase().equals(search.get_placeOfRegistration().toLowerCase()))
                .findAny()
                .orElse(null) != null;
    }
}
