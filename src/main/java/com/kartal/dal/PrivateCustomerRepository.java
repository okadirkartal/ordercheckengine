package com.kartal.dal;

import com.kartal.entities.common.Constants;
import com.kartal.entities.customers.PrivateCustomer;

import java.util.ArrayList;

import org.json.simple.JSONObject;

public class PrivateCustomerRepository extends CustomerRepository<PrivateCustomer> {

    public PrivateCustomerRepository() {
        getCustomers();
    }

    ArrayList<PrivateCustomer> list = new ArrayList<PrivateCustomer>();

    @Override
    public PrivateCustomer getCustomer(String cardId) {

        return list.stream()
                .filter(search -> cardId.equals(search.get_cardId()))
                .findAny()
                .orElse(null);
    }

    @Override
    public boolean IssueCard(PrivateCustomer customer) {

        return list.stream()
                .filter(search -> customer.get_cardId().toLowerCase().equals(search.get_cardId().toLowerCase()))
                .findAny()
                .orElse(null) != null;
    }

    @Override
    public boolean RegistrationCustomer(PrivateCustomer customer) {

        PrivateCustomer returnedCustomer = list.stream()
                .filter(search -> customer.get_placeOfRegistration().toLowerCase().equals(search.get_placeOfRegistration().toLowerCase()))
                .filter(search -> customer.get_cardId().equals(search.get_cardId()))
                .findAny()
                .orElse(null);

        if (returnedCustomer != null) {

            System.out.println("this customer is exists");

            return false;
        }
        return true;
    }

    @Override
    public boolean InquiryCustomer(PrivateCustomer customer) {

        return list.stream()
                .filter(search -> customer.get_placeOfRegistration().toLowerCase().equals(search.get_placeOfRegistration().toLowerCase()))
                .filter(search -> customer.get_cardId().toLowerCase().equals(search.get_cardId().toLowerCase()))
                .findAny()
                .orElse(null) != null;
    }


    public void getParsedCustomer(JSONObject customer) {

        JSONObject obj = (JSONObject) customer.get(Constants.PrivateCustomerTag);

        if (obj != null)
            list.add(new PrivateCustomer(
                    (long) obj.get("Id"), obj.get("cardId").toString(), obj.get("placeOfRegistration").toString(), obj.get("firstName").toString(),
                    obj.get("lastName").toString(), obj.get("birthDate").toString()
            ));
    }
}
