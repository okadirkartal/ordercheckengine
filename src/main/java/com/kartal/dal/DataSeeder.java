package com.kartal.dal;

import com.kartal.entities.common.Constants;
import com.kartal.entities.customers.CorporateCustomer;
import com.kartal.entities.customers.PrivateCustomer;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class DataSeeder {

    private JSONArray customerList = new JSONArray();

    private void fillCorporateCustomers() {

        CorporateCustomer[] corporateCustomers = new CorporateCustomer[]{
                new CorporateCustomer(1l, "55555555555", "United States", "MICROSOFT", "01/01/1990", "818181"),
                new CorporateCustomer(2l, "66666666666", "RUSSIA", "YANDEX", "01/01/1991", "32131"),
                new CorporateCustomer(3l, "77777777777", "LATVIA", "KARTAL", "01/01/1992", "818182")
        };

        for (CorporateCustomer corporateCustomer : corporateCustomers) {
            JSONObject corporateCustomersAsJson = new JSONObject();
            corporateCustomersAsJson.put("Id", corporateCustomer.get_id());
            corporateCustomersAsJson.put("cardId", corporateCustomer.get_cardId());
            corporateCustomersAsJson.put("placeOfRegistration", corporateCustomer.get_placeOfRegistration());
            corporateCustomersAsJson.put("companyName", corporateCustomer.get_companyName());
            corporateCustomersAsJson.put("foundationDate", corporateCustomer.get_foundationDate());
            corporateCustomersAsJson.put("registryNumber", corporateCustomer.get_registryNumber());
            JSONObject obj = new JSONObject();
            obj.put(Constants.CorporateCustomerTag, corporateCustomersAsJson);
            customerList.add(obj);
        }
    }

    private void fillPrivateCustomers() {

        PrivateCustomer[] privateCustomers = new PrivateCustomer[]{
                new PrivateCustomer(1l, "11111111111", "United States", "Yuri", "Vladimir", "01/01/1980"),
                new PrivateCustomer(2l, "22222222222", "Latvia", "Antons", "Ivaska", "01/01/1991"),
                new PrivateCustomer(3l, "33333333333", "Turkey", "Osman", "kartal", "01/01/1992")
        };

        for (PrivateCustomer privateCustomer : privateCustomers) {
            JSONObject privateCustomersAsJson = new JSONObject();
            privateCustomersAsJson.put("Id", privateCustomer.get_id());
            privateCustomersAsJson.put("cardId", privateCustomer.get_cardId());
            privateCustomersAsJson.put("placeOfRegistration", privateCustomer.get_placeOfRegistration());
            privateCustomersAsJson.put("firstName", privateCustomer.get_firstName());
            privateCustomersAsJson.put("lastName", privateCustomer.get_lastName());
            privateCustomersAsJson.put("birthDate", privateCustomer.get_birthDate());
            JSONObject obj = new JSONObject();
            obj.put(Constants.PrivateCustomerTag, privateCustomersAsJson);
            customerList.add(obj);
        }
    }

    public void Seed() {

        fillCorporateCustomers();

        fillPrivateCustomers();

        try (FileWriter file = new FileWriter(Constants.customerDbPath)) {

            file.write(customerList.toJSONString());
            file.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void Clear() {
        try {
            FileWriter fw = new FileWriter(Constants.customerDbPath);
            fw.write("");
            fw.flush();
            fw.close();
            customerList.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
