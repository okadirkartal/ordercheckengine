package com.kartal.dal;

import com.kartal.entities.Place;
import com.kartal.entities.common.Constants;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class PlaceRepository {

    private static PlaceRepository instance;

    private PlaceRepository() {

        populatePlace();
    }

    public static synchronized PlaceRepository getInstance() {

        if (instance == null) {
            instance = new PlaceRepository();
        }
        return instance;
    }

    static ArrayList<Place> list = new ArrayList<Place>();

    public static void populatePlace() {

        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(Constants.placeDbPath)) {
            //Read JSON file
            Object obj = jsonParser.parse(reader);

            JSONArray placeList = (JSONArray) obj;

            //Iterate over employee array
            placeList.forEach(customer -> getParsedPlace((JSONObject) customer));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public boolean getPlace(String placeName) {

        return list.stream()
                .filter(search -> placeName.equals(search.get_name()))
                .findAny()
                .orElse(null) != null;
    }

    private static void getParsedPlace(JSONObject customer) {

        JSONObject obj = (JSONObject) customer;

        if (obj != null)

            list.add(new Place(
                    customer.get("name").toString(), customer.get("code").toString()));
    }
}
