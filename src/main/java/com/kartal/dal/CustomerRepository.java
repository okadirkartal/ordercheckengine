package com.kartal.dal;

import com.kartal.entities.common.Constants;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public abstract class CustomerRepository<T> {

    abstract T getCustomer(String cardId);

    abstract boolean IssueCard(T customer);

    abstract boolean RegistrationCustomer(T customer);

    abstract boolean InquiryCustomer(T customer);

    abstract void getParsedCustomer(JSONObject customer);

    public void getCustomers() {

        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(Constants.customerDbPath)) {
            //Read JSON file
            Object obj = jsonParser.parse(reader);
            JSONArray customerList = (JSONArray) obj;

            //Iterate over employee array
            customerList.forEach(customer -> getParsedCustomer((JSONObject) customer));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


}
