package com.kartal.entities.customers;

public class CustomerBase {

    private long _id;

    private String _cardId;

    private String _placeOfRegistration;

    public CustomerBase(long Id, String cardId, String placeOfRegistration) {
        this._id = Id;
        this._cardId = cardId;
        this._placeOfRegistration = placeOfRegistration;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long id) {
        this._id = id;
    }

    public String get_cardId() {
        return _cardId;
    }

    public void set_cardId(String cardId) {
        this._cardId = cardId;
    }

    public String get_placeOfRegistration() {
        return _placeOfRegistration;
    }

    public void set_placeOfRegistration(String placeOfRegistration) {
        this._placeOfRegistration = placeOfRegistration;
    }
}
