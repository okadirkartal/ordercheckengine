package com.kartal.entities.customers;

public class CorporateCustomer extends CustomerBase {


    private String _companyName;

    private String _foundationDate;

    private String _registryNumber;

    public CorporateCustomer(long Id, String cardId, String placeOfRegistration, String companyName, String foundationDate, String registryNumber) {
        super(Id, cardId, placeOfRegistration);
        this._companyName = companyName;
        this._foundationDate = foundationDate;
        this._registryNumber = registryNumber;
    }


    public void set_companyName(String companyName) {
        this._companyName = companyName;
    }

    public String get_companyName() {
        return _companyName;
    }


    public String get_foundationDate() {
        return _foundationDate;
    }

    public void set_foundationDate(String foundationDate) {
        this._foundationDate = foundationDate;
    }


    public String get_registryNumber() {
        return _registryNumber;
    }

    public void set_registryNumber(String registryNumber) {
        this._registryNumber = registryNumber;
    }

}
