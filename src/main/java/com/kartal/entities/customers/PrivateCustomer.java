package com.kartal.entities.customers;

public class PrivateCustomer extends CustomerBase {

    public PrivateCustomer(long Id, String cardId, String placeOfRegistration, String firstName, String lastName, String birthDate) {
        super(Id, cardId, placeOfRegistration);
        this._firstName = firstName;
        this._lastName = lastName;
        this._birthDate = birthDate;
    }

    private String _firstName;

    private String _lastName;

    private String _birthDate;

    public String get_firstName() {
        return _firstName;
    }

    public void set_firstName(String firstName) {
        this._firstName = firstName;
    }

    public String get_lastName() {
        return _lastName;
    }

    public void set_lastName(String lastName) {
        this._lastName = lastName;
    }

    public String get_birthDate() {
        return _birthDate;
    }

    public void set_birthDate(String birthDate) {
        this._birthDate = birthDate;
    }
}
