package com.kartal.entities;

public class Place {

    public Place(String name, String code) {
        this._name = name;
        this._code = code;
    }

    private String _name;

    private String _code;

    public String get_name() {
        return _name;
    }

    public void set_name(String name) {
        this._name = name;
    }

    public String get_code() {
        return _code;
    }

    public void set_code(String code) {
        this._code = code;
    }
}
