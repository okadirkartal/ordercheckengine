package com.kartal.entities.common;

public class Constants {
    public static final String PrivateCustomerTag = "privateCustomer";
    public static final String CorporateCustomerTag = "corporateCustomer";
    public static final String customerDbPath = "data/customers.json";
    public static final String placeDbPath = "data/place.json";
}
