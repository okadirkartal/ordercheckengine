package com.kartal.entities.enums;

public enum ValidationResult {
    VALID, NOT_VALID
}
