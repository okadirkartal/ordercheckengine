package com.kartal.entities.enums;

public enum CustomerType {
    INDIVIDUAL, CORPORATE
}
