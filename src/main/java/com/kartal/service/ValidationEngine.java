package com.kartal.service;

import com.kartal.dal.CorporateCustomerRepository;
import com.kartal.dal.PrivateCustomerRepository;
import com.kartal.entities.customers.CorporateCustomer;
import com.kartal.entities.customers.PrivateCustomer;
import com.kartal.entities.enums.ValidationResult;

public class ValidationEngine<T> {

    public ValidationResult RegistrationOfCustomer(T customer) {

        if (customer instanceof PrivateCustomer) {

            PrivateCustomerRepository pcr = new PrivateCustomerRepository();

            return pcr.RegistrationCustomer((PrivateCustomer) customer) ?
                    ValidationResult.VALID : ValidationResult.NOT_VALID; //Private Customer is exists
        } else {

            CorporateCustomerRepository ccr = new CorporateCustomerRepository();

            return ccr.RegistrationCustomer((CorporateCustomer) customer) ?
                    ValidationResult.VALID : ValidationResult.NOT_VALID; //Corporate Customer is exists
        }
    }

    public ValidationResult IssueCard(T customer) {

        if (customer instanceof PrivateCustomer) {

            PrivateCustomer privateCustomer = (PrivateCustomer) customer;

            return new DocumentDataValidationService().ValidateCard(privateCustomer.get_cardId()) ?//Check card validity
                    ValidationResult.VALID : ValidationResult.NOT_VALID;
        } else {

            CorporateCustomerRepository ccr = new CorporateCustomerRepository();

            CorporateCustomer corporateCustomer = (CorporateCustomer) customer;

            return ccr.IssueCard((CorporateCustomer) customer) && //Check customer
                    new DocumentDataValidationService().ValidateCard(corporateCustomer.get_cardId()) ?//Check card validity
                    ValidationResult.VALID : ValidationResult.NOT_VALID;
        }
    }

    public ValidationResult InquiryCustomer(T customer) {

        DocumentDataValidationService service = new DocumentDataValidationService();

        if (customer instanceof PrivateCustomer) {

            PrivateCustomerRepository pcr = new PrivateCustomerRepository();

            return pcr.InquiryCustomer((PrivateCustomer) customer) &&
                    service.placeNameIsValid(((PrivateCustomer) customer).get_placeOfRegistration()) ?
                    ValidationResult.VALID : ValidationResult.NOT_VALID;
        } else {

            CorporateCustomerRepository ccr = new CorporateCustomerRepository();

            return ccr.InquiryCustomer((CorporateCustomer) customer) &&
                    service.placeNameIsValid(((CorporateCustomer) customer).get_placeOfRegistration()) ?
                    ValidationResult.VALID : ValidationResult.NOT_VALID;
        }
    }
}
