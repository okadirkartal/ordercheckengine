package com.kartal.service;

import com.kartal.dal.PlaceRepository;

public class DocumentDataValidationService {

    //Check card is valid
    public boolean ValidateCard(String cardId) {

        return cardId != null &&
                cardId.length() == 11;
    }

    //Check place is valid
    public boolean placeNameIsValid(String place) {

        return PlaceRepository.getInstance().getPlace(place);
    }
}
