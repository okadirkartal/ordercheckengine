package com.kartal;

import com.kartal.dal.DataSeeder;
import com.kartal.entities.customers.CorporateCustomer;
import com.kartal.entities.customers.PrivateCustomer;
import com.kartal.service.ValidationEngine;

public class Main {

    public static void main(String[] args) {

        DataSeeder seeder = new DataSeeder();

        //Generate database
        seeder.Seed();

        PrivateCustomer privateCustomer = new PrivateCustomer(1l, "11111111112", "United States", "Yuri", "Vladimir", "01/01/1980");

        ValidationEngine<PrivateCustomer> validationEngineForPrivateCustomer
                = new ValidationEngine<>();

        System.out.println("Results for " + privateCustomer.get_firstName() + " " + privateCustomer.get_lastName());
        System.out.println("Inquiry : " + validationEngineForPrivateCustomer.InquiryCustomer(privateCustomer));
        System.out.println("Issue Card : " + validationEngineForPrivateCustomer.IssueCard(privateCustomer));
        System.out.println("Registration of Customer : " + validationEngineForPrivateCustomer.RegistrationOfCustomer(privateCustomer));


        CorporateCustomer corporateCustomer = new CorporateCustomer(1l, "55555555555", "United States", "MICROSOSFT", "01/01/1990", "818181");
        ValidationEngine<CorporateCustomer> validationEngineForCorporateCustomer
                = new ValidationEngine<>();

        System.out.println("\n\nResults for " + corporateCustomer.get_companyName());
        System.out.println("Inquiry : " + validationEngineForCorporateCustomer.InquiryCustomer(corporateCustomer));
        System.out.println("Issue Card : " + validationEngineForCorporateCustomer.IssueCard(corporateCustomer));
        System.out.println("Registration of Customer : " + validationEngineForCorporateCustomer.RegistrationOfCustomer(corporateCustomer));

        //seeder.Clear();
    }

}
